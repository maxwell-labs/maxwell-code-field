<?php
namespace Craft;

/**
 * Class MaxwellCode_CodeFieldType
 *
 * @author    Maxwell Labs <support@pixelandtonic.com>
 * @copyright Copyright (c) 2015, Maxwell Labs
 */
class MaxwellCode_CodeFieldType extends BaseFieldType
{
	/**
	 * @inheritDoc IComponentType::getName()
	 *
	 * @return string
	 */
	public function getName()
	{
		return Craft::t('Code (Maxwell)');
	}

	/**
	 * @inheritDoc ISavableComponentType::getSettingsHtml()
	 *
	 * @return string|null
	 */
	public function getSettingsHtml()
	{
		$settings = $this->getSettings();
		if($settings->mode == '') { $settings->mode = 'text'; }
		if($settings->theme == '') { $settings->theme = 'ace/theme/solarized_dark'; }
		return craft()->templates->render('maxwellcode/settings', array(
			'settings' => $settings,
			'modes' => $this->modes(),
			'themes' => $this->themes(),
		));
	}

	/**
	 * @inheritDoc IFieldType::getInputHtml()
	 *
	 * @param string $name
	 * @param mixed  $value
	 *
	 * @return string
	 */
	public function getInputHtml($name, $value)
	{
		$settings = $this->getSettings();

		if(!$value instanceof MaxwellCode_CodeModel)
		{
			$value = new MaxwellCode_CodeModel($value, $this->getSettings(), $this->modes(), $this->themes());
		}

		$id = craft()->templates->formatInputId($name);
		$formattedId = craft()->templates->namespaceInputId($id);
		$fieldJs = $this->_getFieldJs($formattedId, $value, $settings);
		$this->_includeFieldResources($fieldJs);
		craft()->templates->includeJs('new Craft.MaxwellCodeInput(' .
			'"'.$formattedId.'", ' .
			'"'.$value->mode.'", ' .
			'"'.$value->theme.'"' .
		');');

		return craft()->templates->render('maxwellcode/code', array(
			'id' => $id,
			'name'  => $name,
			'value' => $value,
		));
	}

	/**
	 * @inheritDoc IFieldType::defineContentAttribute()
	 *
	 * @return mixed
	 */
	public function defineContentAttribute()
	{
		return array(AttributeType::String, 'column' => ColumnType::Text);
	}

	/**
	 * Outputs an object so that syntax can be determined.
	 *
	 * @param string $value
	 * @return string|object
	 */
	public function prepValue($value)
	{
		return new MaxwellCode_CodeModel($value, $this->getSettings(), $this->modes(), $this->themes());
	}

	// Protected Methods
	// =========================================================================

	/**
	 * @inheritDoc BaseSavableComponentType::defineSettings()
	 *
	 * @return array
	 */
	protected function defineSettings()
	{
		return array(
			'mode'  => AttributeType::String,
			'theme'  => AttributeType::String,
		);
	}

	// Private Methods
	// =========================================================================

	/**
	 * Generates JS for this particular field.
	 *
	 * @param string $name
	 * @param mixed  $value
	 *
	 * @return string
	 */
	private function _getFieldJs($name, $value, $settings)
	{
		return craft()->templates->render('maxwellcode/field.js', array(
			'name'  => $name,
			'value' => $value,
			'settings' => $settings,
		));
	}

	/**
	 * Applies input resources.
	 *
	 * @param string $fieldJs
	 *
	 * @return null
	 */
	private function _includeFieldResources()
	{
		craft()->templates->includeCssResource('maxwellcode/code.css');
		craft()->templates->includeJsResource('maxwellcode/ace/ace.js');
		craft()->templates->includeJsResource('maxwellcode/code.js');
	}

	private function modes ()
	{
		return array(
			"abap" => "ABAP",
			"abc" => "ABC",
			"actionscript" => "ActionScript",
			"ada" => "ADA",
			"apache_conf" => "Apache Conf",
			"asciidoc" => "AsciiDoc",
			"assembly_x86" => "Assembly x86",
			"autohotkey" => "AutoHotKey",
			"batchfile" => "BatchFile",
			"c_cpp" => "C and C",
			"c9search" => "C9Search",
			"cirru" => "Cirru",
			"clojure" => "Clojure",
			"cobol" => "Cobol",
			"coffee" => "CoffeeScript",
			"coldfusion" => "ColdFusion",
			"csharp" => "C",
			"css" => "CSS",
			"curly" => "Curly",
			"d" => "D",
			"dart" => "Dart",
			"diff" => "Diff",
			"django" => "Django",
			"dockerfile" => "Dockerfile",
			"dot" => "Dot",
			"eiffel" => "Eiffel",
			"ejs" => "EJS",
			"elixir" => "Elixir",
			"elm" => "Elm",
			"erlang" => "Erlang",
			"forth" => "Forth",
			"ftl" => "FreeMarker",
			"gcode" => "Gcode",
			"gherkin" => "Gherkin",
			"gitignore" => "Gitignore",
			"glsl" => "Glsl",
			"golang" => "Go",
			"groovy" => "Groovy",
			"haml" => "HAML",
			"handlebars" => "Handlebars",
			"haskell" => "Haskell",
			"haxe" => "haXe",
			"html" => "HTML",
			"html_ruby" => "HTML (Ruby",
			"ini" => "INI",
			"io" => "Io",
			"jack" => "Jack",
			"jade" => "Jade",
			"java" => "Java",
			"javascript" => "JavaScript",
			"json" => "JSON",
			"jsoniq" => "JSONiq",
			"jsp" => "JSP",
			"jsx" => "JSX",
			"julia" => "Julia",
			"latex" => "LaTeX",
			"lean" => "Lean",
			"less" => "LESS",
			"liquid" => "Liquid",
			"lisp" => "Lisp",
			"livescript" => "LiveScript",
			"logiql" => "LogiQL",
			"lsl" => "LSL",
			"lua" => "Lua",
			"luapage" => "LuaPage",
			"lucene" => "Lucene",
			"makefile" => "Makefile",
			"markdown" => "Markdown",
			"mask" => "Mask",
			"matlab" => "MATLAB",
			"maze" => "Maze",
			"mel" => "MEL",
			"mushcode" => "MUSHCode",
			"mysql" => "MySQL",
			"nix" => "Nix",
			"objectivec" => "Objective-C",
			"ocaml" => "OCaml",
			"pascal" => "Pascal",
			"perl" => "Perl",
			"pgsql" => "pgSQL",
			"php" => "PHP",
			"powershell" => "Powershell",
			"praat" => "Praat",
			"prolog" => "Prolog",
			"properties" => "Properties",
			"protobuf" => "Protobuf",
			"python" => "Python",
			"r" => "R",
			"rdoc" => "RDoc",
			"rhtml" => "RHTML",
			"ruby" => "Ruby",
			"rust" => "Rust",
			"sass" => "SASS",
			"scad" => "SCAD",
			"scala" => "Scala",
			"scheme" => "Scheme",
			"scss" => "SCSS",
			"sh" => "SH",
			"sjs" => "SJS",
			"smarty" => "Smarty",
			"snippets" => "snippets",
			"soy_template" => "Soy Template",
			"space" => "Space",
			"sql" => "SQL",
			"sqlserver" => "SQLServer",
			"stylus" => "Stylus",
			"svg" => "SVG",
			"tcl" => "Tcl",
			"tex" => "Tex",
			"text" => "Text",
			"textile" => "Textile",
			"toml" => "Toml",
			"twig" => "Twig",
			"typescript" => "Typescript",
			"vala" => "Vala",
			"vbscript" => "VBScript",
			"velocity" => "Velocity",
			"verilog" => "Verilog",
			"vhdl" => "VHDL",
			"xml" => "XML",
			"xquery" => "XQuery",
			"yaml" => "YAML",
		);
	}

	private function themes()
	{
		return array(
			"all" => array(
				"ace/theme/ambiance" => "Ambiance",
				"ace/theme/chaos" => "Chaos",
				"ace/theme/chrome" => "Chrome",
				"ace/theme/clouds_midnight" => "Clouds Midnight",
				"ace/theme/clouds" => "Clouds",
				"ace/theme/cobalt" => "Cobalt",
				"ace/theme/crimson_editor" => "Crimson Editor",
				"ace/theme/dawn" => "Dawn",
				"ace/theme/dreamweaver" => "Dreamweaver",
				"ace/theme/eclipse" => "Eclipse",
				"ace/theme/github" => "GitHub",
				"ace/theme/idle_fingers" => "idle Fingers",
				"ace/theme/iplastic" => "IPlastic",
				"ace/theme/katzenmilch" => "KatzenMilch",
				"ace/theme/kr_theme" => "krTheme",
				"ace/theme/kuroir" => "Kuroir",
				"ace/theme/merbivore_soft" => "Merbivore Soft",
				"ace/theme/merbivore" => "Merbivore",
				"ace/theme/mono_industrial" => "Mono Industrial",
				"ace/theme/monokai" => "Monokai",
				"ace/theme/pastel_on_dark" => "Pastel on dark",
				"ace/theme/solarized_dark" => "Solarized Dark",
				"ace/theme/solarized_light" => "Solarized Light",
				"ace/theme/sqlserver" => "SQL Server",
				"ace/theme/terminal" => "Terminal",
				"ace/theme/textmate" => "TextMate",
				"ace/theme/tomorrow_night_blue" => "Tomorrow Night Blue",
				"ace/theme/tomorrow_night_bright" => "Tomorrow Night Bright",
				"ace/theme/tomorrow_night_eighties" => "Tomorrow Night 80s",
				"ace/theme/tomorrow_night" => "Tomorrow Night",
				"ace/theme/tomorrow" => "Tomorrow",
				"ace/theme/twilight" => "Twilight",
				"ace/theme/vibrant_ink" => "Vibrant Ink",
				"ace/theme/xcode" => "XCode",
			),
			"light" => array(
				"ace/theme/chrome" => "Chrome",
				"ace/theme/clouds" => "Clouds",
				"ace/theme/crimson_editor" => "Crimson Editor",
				"ace/theme/dawn" => "Dawn",
				"ace/theme/dreamweaver" => "Dreamweaver",
				"ace/theme/eclipse" => "Eclipse",
				"ace/theme/github" => "GitHub",
				"ace/theme/iplastic" => "IPlastic",
				"ace/theme/solarized_light" => "Solarized Light",
				"ace/theme/textmate" => "TextMate",
				"ace/theme/tomorrow" => "Tomorrow",
				"ace/theme/xcode" => "XCode",
				"ace/theme/kuroir" => "Kuroir",
				"ace/theme/katzenmilch" => "KatzenMilch",
				"ace/theme/sqlserver" => "SQL Server",
			),
			"dark" => array(
				"ace/theme/ambiance" => "Ambiance",
				"ace/theme/chaos" => "Chaos",
				"ace/theme/clouds_midnight" => "Clouds Midnight",
				"ace/theme/cobalt" => "Cobalt",
				"ace/theme/idle_fingers" => "idle Fingers",
				"ace/theme/kr_theme" => "krTheme",
				"ace/theme/merbivore" => "Merbivore",
				"ace/theme/merbivore_soft" => "Merbivore Soft",
				"ace/theme/mono_industrial" => "Mono Industrial",
				"ace/theme/monokai" => "Monokai",
				"ace/theme/pastel_on_dark" => "Pastel on dark",
				"ace/theme/solarized_dark" => "Solarized Dark",
				"ace/theme/terminal" => "Terminal",
				"ace/theme/tomorrow_night" => "Tomorrow Night",
				"ace/theme/tomorrow_night_blue" => "Tomorrow Night Blue",
				"ace/theme/tomorrow_night_bright" => "Tomorrow Night Bright",
				"ace/theme/tomorrow_night_eighties" => "Tomorrow Night 80s",
				"ace/theme/twilight" => "Twilight",
				"ace/theme/vibrant_ink" => "Vibrant Ink",
			)
		);
	}
}
