<?php
namespace Craft;

/**
 * Class MaxwellCode_CodeModel
 *
 * This is a chintzy model meant for adding features to code fields.
 *
 * @package Craft
 */
class MaxwellCode_CodeModel
{
	public $mode = '';
	public $theme = '';
	public $text = '';
	public $modes = array();
	public $themes = array();

	function __construct($value, $settings, $modes, $themes)
	{
		/**
		 * We're storing our code as JSON so we can store the
		 * language and theme. We fall back to field defaults if it's not set.
		 */
		$data = json_decode($value);
		if(isset($data->mode) && $data->mode != '')
		{
			$this->mode = $data->mode;
		}
		else
		{
			$this->mode = $settings->mode;
		}
		if(isset($data->theme) && $data->theme != '')
		{
			$this->theme = $data->theme;
		}
		else
		{
			$this->theme = $settings->theme;
		}
		if(isset($data->val) && $data->val != '')
		{
			$this->text = $data->val;
		}

		// Set our options for front-end editor use.
		$this->modes = $modes;
		$this->themes = $themes;
	}

	/**
	 * hands out JSON for CP requests, and the raw code otherwise.
	 *
	 * @return string
	 */
	function __toString()
	{
		if (craft()->request->isCpRequest())
		{
			return json_encode(array(
				"mode" => $this->mode,
				"theme" => $this->theme,
				"val" => $this->text,
			));
		}
		else
		{
			return $this->text;
		}
	}

	/**
	 * Adds Javascript for viewing code in Ace.
	 *
	 * @param string $name
	 *
	 * @return null
	 */
	public function includeViewJs($name)
	{
		craft()->templates->includeJsResource('maxwellcode/ace/ace.js');
		craft()->templates->includeJs(
			'var '.$name.'Editor = ace.edit("'.$name.'");'."\n".
			$name.'Editor.setTheme("'.$this->theme.'");'."\n".
			$name.'Editor.getSession().setMode("ace/mode/'.$this->mode.'");'."\n".
			$name.'Editor.session.setUseWorker(false);'."\n".
			$name.'Editor.setReadOnly(true);'
		);
	}

	/**
	 * Adds Javascript for editing code in Ace.
	 *
	 * @param string $name
	 *
	 * @return null
	 */
	public function includeEditJs($name)
	{
		craft()->templates->includeJsResource('maxwellcode/ace/ace.js');
		craft()->templates->includeJs(
			'var '.$name.'Editor = ace.edit("'.$name.'");'."\n".
			$name.'Editor.setTheme("'.$this->theme.'");'."\n".
			$name.'Editor.getSession().setMode("ace/mode/'.$this->mode.'");'
		);
	}
}
