<?php
namespace Craft;

/**
 * Class MaxwellCodePlugin
 *
 * @package Craft
 */
class MaxwellCodePlugin extends BasePlugin
{
	function getName()
	{
		return Craft::t('Maxwell Code Field');
	}

	function getVersion()
	{
		return '0.5';
	}

	function getDeveloper()
	{
		return 'Maxwell Labs';
	}

	function getDeveloperUrl()
	{
		return 'https://maxwell-labs.com';
	}
}
