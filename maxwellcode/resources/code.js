Craft.MaxwellCodeInput = Garnish.Base.extend(
	{
		id: null,
		$textarea: null,
		$toolbar: null,
		ace: null,

		init: function(id, mode, theme) {
			this.id = id;
			this.mode = mode;
			this.theme = theme;

			// Initialize Ace
			this.$textarea = $('#'+this.id);
			this.$toolbar = $('#'+this.id+'-toolbar');
			this.ace = ace.edit(this.id+'-editor');
			this.ace.setTheme(this.theme);
			this.ace.getSession().setMode("ace/mode/"+this.mode);

			this.ace.on('change', function(e, editor) {
				$(editor.container).next().val(editor.getValue()).change();
			});
			$('#'+this.id+'-temp').on('change', { codeInput: this }, function(e) {
				e.data.codeInput.updateField(e.data.codeInput);
			});
			this.$toolbar.on('change', '.ace-mode', { codeInput: this }, function(e) {
				var mode = $(this).val();
				e.data.codeInput.mode = mode;
				e.data.codeInput.ace.getSession().setMode("ace/mode/"+mode);
				e.data.codeInput.updateField(e.data.codeInput);
			});
			this.$toolbar.on('change', '.ace-theme', { codeInput: this }, function(e) {
				var theme = $(this).val();
				e.data.codeInput.theme = theme;
				e.data.codeInput.ace.setTheme(theme);
				e.data.codeInput.updateField(e.data.codeInput);
			});
		},
		updateField: function(codeInput) {
			codeInput.$textarea.val(JSON.stringify(
				{
					mode: codeInput.mode,
					theme: codeInput.theme,
					val: codeInput.ace.getValue(),
				}
			));
		}
	}
)
