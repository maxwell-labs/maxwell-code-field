# Code Field Type by Maxwell Labs

This is a field type for Craft CMS. It's presents a code editor (powered by Ace) in the control panel. It can be used for adding code samples to documentation and blogs or writing raw HTML (though you should probably avoid that for client sites).

## Installation

Just upload the `maxwellcode` folder to your site's plugins folder. The plugin should then be present in your plugins page under settings.

## Usage

### Control Panel

When you create a code field, you'll be given two settings.

* **Default Mode** The default language syntax to use if one isn't chosen.
* **Default Theme** The default theme to use if one isn't chosen.

### Front-End

When templating, you are given a Code Model. If you try to output it like any other field, you'll just get the raw code. You'll also have access to the the mode setting and theme when you last saved the field. This can be useful if you are using a syntax highlighting library in your template.

For convenience, the functions `includeViewJs` and `includeEditJs` have been added to the model. It outputs the Ace library and the javascript needed to activate syntax highlighting. They are mostly the same except `includeViewJs` makes the code **read-only** and disables workers (which does syntax checking).

To use the functions, you need to wrap the code in a div with an id. Then pass the id to the function. An example is below.

#### Read Only Example with embedded Javascript

	{% do entry.codeField.includeViewJs('codeSample') %}
	<div id="codeSample">{{ entry.codeField }}</div>

For front-end editing, you'll need to pass a JSON string to Craft with a `val` field containing your code. You could also include the `mode` and `theme` fields if you want those specifically defined. You can access the complete list (an array) of modes with `codeField.modes`. `codeField.themes` with give you three arrays:

* **codeField.themes.all** will return the whole list of syntax themes.
* **codeField.themes.light** will return only light-colored themes.
* **codeField.themes.dark** will return only dark-colored themes.
